# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.




def guessing_game
  number = rand(1..100)
  puts "Guess a number between 1 and 100"
  guess = gets.chomp.to_i
  previous_guess = 0
  number_of_guesses = 1
  until guess == number
    if guess < number
      puts "#{guess} too low"
    elsif guess > number
      puts "#{guess} too high"
    elsif guess < 1 || guess >100
      raise "guess must be between 1 and 100"
    elsif guess == previous_guess
      raise "you've already guessed that number, try again"
    end
    previous_guess = guess
    number_of_guesses +=1
    puts "guess again"
    guess = gets.chomp.to_i
  end
  puts "#{guess} You guessed in #{number_of_guesses} guesses"

end
